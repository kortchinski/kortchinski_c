﻿using System;
using NUnit.Framework;
using Book = BookEntity.Book;


namespace BookEntity.Tests
{
    [TestFixture]
    public class BookTests
    {
        private Book _book;
        
        [SetUp]
        public void SetUp()
        {
            _book = new Book("Jon Skeet","C# in Depth","Manning Publications");
        }
        
        [TestCase(2010,10,12)]
        [TestCase(2002,11,10)]
        public void TestPublish(int publishYear, int publishMonth, int publishDay)
        {
            DateTime date = new DateTime(publishYear, publishMonth, publishDay);
            _book.Publish(date);
            Assert.AreEqual(_book.GetPublicationDate(), $"{publishMonth}/{publishDay}/{publishYear}");

        }

        [TestCase(39.99,"USD")]
        [TestCase(158.78,"RUS")]
        public void TestSetPrice(decimal price, string currency)
        {
            _book.SetPrice(price,currency);
            Assert.IsTrue(price == _book.Price && currency == _book.Currency);
        }
        
        [TestCase(-39.99,"USD")]
        [TestCase(-158.78,"RUS")]
        public void TestSetPriceIncorrect(decimal price, string currency)
        {
            Assert.Throws<ArgumentException>(delegate { _book.SetPrice(price, currency);});
        }

        
        [Test]
        public void TestCompareTo()
        {
            Book bookToCompare = new Book("Pushkin", "Hello world", "Manning Publications");
            Assert.IsTrue(bookToCompare.CompareTo(_book) < 0);
        }
        
        [Test]
        public void TestCompareToOther()
        {
            Book bookToCompare = new Book("Jon Skeet", "C# in Depth", "Manning Publications");
            Assert.IsTrue(bookToCompare.CompareTo(_book) == 0);
        }

        [TestCase("ShortAuthor", "Very long Author", ExpectedResult = -1)]
        [TestCase("Very long Author", "ShortAuthor", ExpectedResult = 1)]
        public int TestAuthorComparator(string firstAuthor, string secondAuthor)
        {
            return new BookAuthorComparator()
                .Compare(
                    new Book(firstAuthor, string.Empty, string.Empty),
                    new Book(secondAuthor, string.Empty, string.Empty)
                );

        }
        
        [TestCase(123, 3211, ExpectedResult = -1)]
        [TestCase(1024, 1024, ExpectedResult = 0)]
        [TestCase(3211, 123, ExpectedResult = 1)]
        public int TestPagesComparator(int firstBookPages, int secondBookPages)
        {
            return new BookPagesComparator()
                .Compare(
                    new Book(string.Empty, string.Empty, string.Empty) { Pages = firstBookPages },
                    new Book(string.Empty, string.Empty, string.Empty) { Pages = secondBookPages }
                );
        }
        
        
        [TestCase(15, "UAH", 20, "USD", ExpectedResult = -1)]
        [TestCase(10, "USD", 10, "USD", ExpectedResult = 0)]
        [TestCase(123, "USD", 15, "USD", ExpectedResult = 1)]
        public int TestPriceComparator(int firstBookPrice, string firstBookPriceCurrency, int secondBookPrice, string secondBookPriceCurrency)
        {
            Book firstBook = new Book(string.Empty, string.Empty, string.Empty);
            firstBook.SetPrice(firstBookPrice, firstBookPriceCurrency);

            Book secondBook = new Book(string.Empty, string.Empty, string.Empty);
            secondBook.SetPrice(secondBookPrice, secondBookPriceCurrency);

            return new BookPriceComparator().Compare(firstBook, secondBook);
        }


    }
}