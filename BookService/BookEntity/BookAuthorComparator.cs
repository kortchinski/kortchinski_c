﻿using System.Collections.Generic;

namespace BookEntity
{
    public class BookAuthorComparator : IComparer<Book>
    {
        public int Compare(Book firstBook, Book secondBook)
        {
            return firstBook.Author.CompareTo(secondBook.Author);
        }
    }
}