﻿using System.Collections.Generic;

namespace BookEntity
{
    public class BookPagesComparator : IComparer<Book>
    {
        public int Compare(Book firstBook, Book secondBook)
        {
            return firstBook.Pages.CompareTo(secondBook.Pages);
        }
    }
}